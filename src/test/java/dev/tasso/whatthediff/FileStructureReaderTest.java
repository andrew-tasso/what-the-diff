/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff;

import dev.tasso.whatthediff.hashing.HashGenerator;
import dev.tasso.whatthediff.hashing.MD5HashGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.nio.file.Path;
import java.nio.file.Paths;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("FileStructureReader")
class FileStructureReaderTest {

	@Test
	@DisplayName("reads a file structure")
	void readFileStructure() throws Exception {

		String basePathString = "./src/test/resources/test-files";
		FileStructure expectFileStructure = setupExpectedFileStructure(basePathString);

		Path basePath = Paths.get(basePathString);
		HashGenerator md5HashGenerator = new MD5HashGenerator();

		FileStructure actualFileStructure = FileStructureReader.readFileStructure(basePath, md5HashGenerator);

		//TODO improve this test. Maybe once the string representation of the base path is removed from the file structure?
		assertThat(actualFileStructure.getPaths()).hasSameElementsAs(expectFileStructure.getPaths());

	}

	private static FileStructure setupExpectedFileStructure(String theBasePathString) {
		FileStructure fileStructure = new FileStructure(Paths.get(theBasePathString));

		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-1/test-file-1.txt"),"e6f1fc3df523a15a83c0d0c540a834b3");
		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-1/test-file-2.txt"),"953f1cc9555e5070b59834399e3dec71");
		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-1/test-file-3.txt"),"0f3b73ccc99e19833d8e22ddd2cce25b");
		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-2/test-file-1.txt"),"e6f1fc3df523a15a83c0d0c540a834b3");
		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-2/test-file-2.txt"),"953f1cc9555e5070b59834399e3dec71");
		fileStructure.addFile(Paths.get(theBasePathString, "/sub-path-2/test-file-3.txt"),"0f3b73ccc99e19833d8e22ddd2cce25b");

		return fileStructure;
	}

}