/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("ProgressDisplay")
class ProgressDisplayTest {

	@Test
	@DisplayName("update correctly displays progress for 1000 items 20 columns wide")
	void update_1000Items() {

		runUpdateTest(1000, 20);

	}

	@Test
	@DisplayName("update correctly displays progress for 10 items 20 columns wide")
	void update_10Items() {

		runUpdateTest(10, 20);

	}

	@Test
	@DisplayName("update correctly displays progress for 47 items 20 columns wide")
	void update_47Items() {

		runUpdateTest(47, 20);

	}

	@Test
	@DisplayName("update correctly displays progress for 289 items 20 columns wide")
	void update_289Items() {

		runUpdateTest(289, 20);

	}

	private void runUpdateTest(int theNumberOfItems, int theNumberOfColumns) {

		ProgressDisplay progressDisplay = new ProgressDisplay(theNumberOfItems, theNumberOfColumns);

		for(int i = 0; i < theNumberOfItems; i++) {

			progressDisplay.update();

		}

	}
}