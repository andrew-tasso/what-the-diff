/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff.io;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("FileIO")
class FileIOTest {

	@Test
	@DisplayName("gets a single file from a path containing only a single file")
	void getFileList_singleFile_listWithSingleFile() throws Exception {

		List<Path> expectedFiles = new ArrayList<>();
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-1/test-file-1.txt"));

		List<Path> actualFiles = FileIO.getFileList(Paths.get("./src/test/resources/test-files/sub-path-1/test-file-1.txt"));

		assertThat(actualFiles).hasSameSizeAs(expectedFiles)
						.hasSameElementsAs(expectedFiles);

	}

	@Test
	@DisplayName("gets a list of files from a path containing nested folders with multiple files")
	void getFileList_fileTree_listWithAllFiles() throws Exception {

		List<Path> expectedFiles = new ArrayList<>();
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-1/test-file-1.txt"));
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-1/test-file-2.txt"));
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-1/test-file-3.txt"));
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-2/test-file-1.txt"));
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-2/test-file-2.txt"));
		expectedFiles.add(Paths.get("./src/test/resources/test-files/sub-path-2/test-file-3.txt"));

		List<Path> actualFiles = FileIO.getFileList(Paths.get("./src/test/resources/test-files/"));

		assertThat(actualFiles).hasSameSizeAs(expectedFiles)
						.hasSameElementsAs(expectedFiles);

	}

}