/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff.hashing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("MD5HashGenerator")
class MD5HashGeneratorTest {

	@Test
	@DisplayName("generates a hash from a valid input file")
	void generateHash_validInput_validHash() throws IOException {

		String expectedMd5Hash = "e6f1fc3df523a15a83c0d0c540a834b3";
		Path inputFilePath = Paths.get("./src/test/resources/test-files/sub-path-1/test-file-1.txt");

		String actualMd5Hash = new MD5HashGenerator().generateHash(inputFilePath);

		assertEquals(expectedMd5Hash, actualMd5Hash);

	}

	@Test
	@DisplayName("throws an IOException when a path that does not exist is provided")
	void generateHash_invalidPath_exceptionThrown() {

		Path inputFilePath = Paths.get("./an-invalid-path/non-existent-file.txt");

		assertThrows(IOException.class, () -> new MD5HashGenerator().generateHash(inputFilePath));


	}
}