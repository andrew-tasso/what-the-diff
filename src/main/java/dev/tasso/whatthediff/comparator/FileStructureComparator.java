/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff.comparator;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.tasso.whatthediff.FileStructure;

/**
 * @author Andrew Tasso
 */
public class FileStructureComparator {

	public static List<Path> getMissingFiles(FileStructure theSourceFileStructure, FileStructure theDestinationFileStructure) {

		List<Path> missingFileList = new ArrayList<>();

		Collection<Path> sourcePaths = theSourceFileStructure.getPaths();
		Collection<Path> destinationPaths = theDestinationFileStructure.getPaths();

		for (Path currPath : sourcePaths) {

			if (!destinationPaths.contains(currPath)) {

				missingFileList.add(currPath);

			}

		}

		return missingFileList;

	}

	public static List<Path> getAdditionalFiles(FileStructure theSourceFileStructure, FileStructure theDestinationFileStructure) {

		List<Path> additionalFileList = new ArrayList<>();

		Collection<Path> sourcePaths = theSourceFileStructure.getPaths();
		Collection<Path> destinationPaths = theDestinationFileStructure.getPaths();


		for (Path currPath : destinationPaths) {

			if (!sourcePaths.contains(currPath)) {

				additionalFileList.add(currPath);

			}

		}


		return additionalFileList;

	}

	public static Map<Path, String> getHashMismatches(FileStructure theSourceFileStructure, FileStructure theDestinationFileStructure) {

		Map<Path, String> hashMismatchMap = new HashMap<>();

		Collection<Path> sourceFileList = theSourceFileStructure.getPaths();
		Collection<Path> destinationFileList = theDestinationFileStructure.getPaths();

		String sourceHash;
		String destHash;

		for (Path currFile : sourceFileList) {

			sourceHash = theSourceFileStructure.getFileHash(currFile);

			if (destinationFileList.contains(currFile)) {

				destHash = theDestinationFileStructure.getFileHash(currFile);

				if (!sourceHash.equals(destHash)) {

					hashMismatchMap.put(currFile, destHash);

				}

			}

		}

		return hashMismatchMap;

	}

}
