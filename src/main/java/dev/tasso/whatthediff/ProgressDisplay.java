/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff;

public class ProgressDisplay {

	private final int numberOfItems;
	private int currentCount;
	private int lastPercent;
	private final int numberOfColumns;
	private int currColumnIndex;

	public ProgressDisplay(int theNumberOfItems, int theNumberOfColumns) {

		this.currColumnIndex = 0;
		this.currentCount = 0;
		this.lastPercent = 0;
		this.numberOfColumns = theNumberOfColumns;
		this.numberOfItems = theNumberOfItems;

	}

	public void update() {

		this.currentCount++;

		int currPercent = ((this.currentCount * 100) / (this.numberOfItems));

		if(currPercent > this.lastPercent) {

			this.lastPercent = currPercent;
			this.currColumnIndex++;

			System.out.printf("%02d%% ", currPercent);

			if(this.currColumnIndex % this.numberOfColumns == 0) {
				this.currColumnIndex = 0;
				System.out.println();
			}

			//Catch cases where the last number doesn't fall at the end of a column
			if(currPercent == 100 && (this.currColumnIndex % this.numberOfColumns != 0)) {
				System.out.println();
			}

		}

	}

}
