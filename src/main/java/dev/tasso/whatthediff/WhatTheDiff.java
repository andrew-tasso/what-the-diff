/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import dev.tasso.whatthediff.comparator.FileStructureComparator;
import dev.tasso.whatthediff.hashing.HashGenerator;
import dev.tasso.whatthediff.hashing.MD5HashGenerator;

/**
 * @author Andrew Tasso
 */
class WhatTheDiff {


	public static void main(String[] args) {
		Path sourcePath = Paths.get(args[0]).toAbsolutePath();
		Path destinationPath = Paths.get(args[1]).toAbsolutePath();
		Path resultsFilePath = args.length >= 3 ? Paths.get(args[2]).toAbsolutePath() : Paths.get( "results.diff").toAbsolutePath();

		System.out.println("Source Path: " + sourcePath);
		System.out.println("Destination Path: " + destinationPath);
		System.out.println("Results File: " + resultsFilePath);

		HashGenerator hashGenerator = new MD5HashGenerator();

		FileStructure sourceFileStructure;
		FileStructure destinationFileStructure;

		try {

			BufferedWriter writer = Files.newBufferedWriter(resultsFilePath);

			List<Path> missingFileList;
			List<Path> additionalFileList;
			Map<Path, String> hashMismatchMap;

			sourceFileStructure = FileStructureReader.readFileStructure(sourcePath, hashGenerator);
			destinationFileStructure = FileStructureReader.readFileStructure(destinationPath, hashGenerator);

			missingFileList = FileStructureComparator.getMissingFiles(sourceFileStructure, destinationFileStructure);

			writer.write("Missing file list: \n");
			for (Path currFile : missingFileList) {

				writer.write(currFile + "\n");

			}
			writer.write("\n");


			additionalFileList = FileStructureComparator.getAdditionalFiles(sourceFileStructure, destinationFileStructure);
			writer.write("Additional file list: \n");
			for (Path currFile : additionalFileList) {

				writer.write(currFile + "\n");

			}
			writer.write("\n");

			hashMismatchMap = FileStructureComparator.getHashMismatches(sourceFileStructure, destinationFileStructure);

			writer.write("Hash Mismatches: \n");
			for (Path currKey : hashMismatchMap.keySet()) {

				writer.write(currKey + " " + hashMismatchMap.get(currKey) + "\n");

			}
			writer.write("\n");
			writer.close();

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
