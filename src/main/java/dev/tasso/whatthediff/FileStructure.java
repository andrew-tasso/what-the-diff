/*
MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package dev.tasso.whatthediff;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Andrew Tasso
 */
public class FileStructure implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1096370709013401484L;

	private final Path basePath;

	private final Map<Path, String> filePathHashMap;

	public FileStructure(Path theBasePath) {

		this.basePath = theBasePath;
		this.filePathHashMap = new LinkedHashMap<>();

	}

	public void addFile(Path theFilePath, String theFileHash) {

		Path relativePath = basePath.relativize(theFilePath);
		this.filePathHashMap.put(relativePath, theFileHash);

	}

	public Collection<Path> getPaths() {
		return this.filePathHashMap.keySet();
	}

	public String getFileHash(Path theFilePath) {

		return this.filePathHashMap.get(theFilePath);

	}

}
