# WhatTheDiff

## License

### MIT License

Copyright (c) 2006-2020 Andrew A. Tasso

See [LICENSE.txt](LICENSE.txt) in the root of this project for the full terms of the license.

### Third Party
This project utilizes 3rd party libraries that are distributed under their own terms. For a complete list of libraries 
and coordinating licenses see [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt) in the root of this project.